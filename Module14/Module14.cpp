﻿
#include <iostream>
using namespace std;
 char LastSymbol(string);
 char FirstSymbol(string);

int main()
{
    string name ("Edward Miterev");
    cout << name << endl << name.length() << endl;
    cout << LastSymbol(name) << endl;
    cout << FirstSymbol(name) << endl;

    return 0;
}

char FirstSymbol(string x)
{
    return x[0];
}
char LastSymbol(string x)
{
    return x[x.length() -1 ];
}